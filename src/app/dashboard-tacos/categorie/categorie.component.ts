import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Categorie } from 'src/app/models/categorie';
import { CategorieService } from 'src/app/services/categorie.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {
  public categories:Categorie[] = [];
  public categorie:Categorie
  public c:Categorie
  selectedFiles: FileList;
  categorieUpdate : Categorie
  currentFile: File;
  progress = 0;
  message = '';
  currentFileUpdate :File;
  public id:number
  fileInfos: Observable<any>;
  isLoggedIn=false
  public showAdminBoard:boolean=false;
  private roles: string[];
  public host:any=environment.apiHost+"/images/";
  constructor(private tokenStorageService:TokenStorageService, private _router: Router, private categorieService:CategorieService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    console.log("hhhhhhh",this.isLoggedIn)
    this.showAdminBoard = this.roles.includes('ROLE_ADMIN');

    if (!this.showAdminBoard) {
      this._router.navigateByUrl("loginAdmin")
    }else{
      this.getCategories()
    }
    
  }

  public getCategories(){
    this.categorieService.tous_categories().subscribe(data=>{
      this.categories = data;
      console.log(data);
    },err=>{ console.log(err);})
   
  }

  public addCategorie(c:Categorie){
    this.progress = 0;
    this.currentFile = this.selectedFiles.item(0);
    console.log(this.currentFile )
    this.c = c
    this.categorieService.upload(this.c,this.currentFile ).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
   
        }
        this.getCategories();
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.currentFile = null;
      });
  
    this.selectedFiles = null;
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  public deleteCateg(id){
   
    
    console.log("hhhhhhh"+id)
    let conf= confirm("etes vous sur de la supprimer ?");
    if(conf){
      this.categorieService.delete(id).subscribe(data=>{
        console.log(data);
        this.getCategories();
        
      },err=>{console.log(err);})
      
    }
      
    
  }

  getExtraUpdate(id:number){
   
    console.log("id"+id )
    this.categorieService.findOne(id).subscribe(data=>{
      console.log(data);
      this.id=id;
      this.categorieUpdate = data;
    },err=>(console.log(err)));
  }
 
  updateExtra(value:Categorie){
    console.log("******** ")
    this.progress = 0;
    this.currentFileUpdate = this.selectedFiles.item(0);
    console.log(this.currentFileUpdate )
    let cat:Categorie = value;
    cat.id=this.id
    //ingrd = new Ingredient(this.id, value.nameIngredient, value.type);
    console.log("******** ",cat);
   // ingrd = new Ingredient(this.id, value.nameIngredient, value.nameIngredient, value.nameIngredient);
    this.categorieService.update(cat,this.currentFileUpdate).subscribe(event=>{
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.message = event.body.message;
      }
      this.getCategories()
    },err=>{
      this.progress = 0;
      this.message = 'Could not upload the file!';
      this.currentFileUpdate = null;
    });
    this.selectedFiles = null;
  }

}
