import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Extras } from 'src/app/models/extras';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { ExtrasService } from 'src/app/services/extras.service';
import { environment } from 'src/environments/environment';
import { CategorieService } from 'src/app/services/categorie.service';
import { Categorie } from 'src/app/models/categorie';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-extras',
  templateUrl: './extras.component.html',
  styleUrls: ['./extras.component.scss']
})
export class ExtrasComponent implements OnInit {
public id : string
  public extras:Extras[] = [];
  public extrasf:Extras
  public e:Extras
  extraUpdate :Extras
  public categories:Categorie[];
  public categorie:Categorie;
  selectedFiles: FileList;
  currentFile: File;
  currentFileUpdate: File;
  progress = 0;
  message = '';
  isLoggedIn=false
  fileInfos: Observable<any>;
  public showAdminBoard:boolean=false;
  private roles: string[];

  public host:any=environment.apiHost+"/images/";
  constructor(private router: Router,private tokenStorageService: TokenStorageService,
     private extrasService : ExtrasService, private categorieService:CategorieService,
     private _router:Router) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    console.log("hhhhhhh",this.isLoggedIn)
    this.showAdminBoard = this.roles.includes('ROLE_ADMIN');

    if (!this.showAdminBoard) {
      this._router.navigateByUrl("loginAdmin")
    }else{
      this.getExtras();
    this.getCategories();
    }
    
  }

  public getExtras(){
    this.extrasService.tous_extras().subscribe(data=>{
      this.extras = data;
      console.log(data);
    },err=>{ console.log(err);})
  }

  public getCategories(){
    this.categorieService.tous_categories().subscribe(data=>{
      this.categories = data;
    },err=>{ console.log(err);})
  }

  public addExtras(e:Extras){
    this.progress = 0;
    this.currentFile = this.selectedFiles.item(0);
    console.log(this.currentFile )
    this.e = e
    this.extrasService.upload(this.e,this.currentFile ).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
   
        }
        this.getExtras();
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.currentFile = null;
      });
  
    this.selectedFiles = null;
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  public deleteExtra(id){
    console.log("hhhhhhh"+id)
    let conf= confirm("etes vous sur de la supprimer ?");
    if(conf){
      this.extrasService.delete(id).subscribe(data=>{
        console.log(data);
        this.extras = this.extras.filter(e=> e.id != id)
      },err=>{console.log(err);})
    }
  }

  onSelectedCategorie(categorie: Categorie){
    console.log(categorie);
    this.categorie = categorie;
  }

  getExtraUpdate(id:string){
   
    console.log("id"+id )
    this.extrasService.findOne(id).subscribe(data=>{
      console.log(data);
      this.id=id;
      this.extraUpdate = data;
    },err=>(console.log(err)));
  }
 
  updateExtra(value:Extras){
    console.log("******** ")
    this.progress = 0;
    this.currentFileUpdate = this.selectedFiles.item(0);
    console.log(this.currentFileUpdate )
    let extras:Extras = value;
    extras.id=this.id
    //ingrd = new Ingredient(this.id, value.nameIngredient, value.type);
    console.log("******** ",extras);
   // ingrd = new Ingredient(this.id, value.nameIngredient, value.nameIngredient, value.nameIngredient);
    this.extrasService.update(extras,this.currentFileUpdate).subscribe(event=>{
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.message = event.body.message;
      }
      this.getExtras()
    },err=>{
      this.progress = 0;
      this.message = 'Could not upload the file!';
      this.currentFileUpdate = null;
    });
    this.selectedFiles = null;
  }
}
