import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ingredient } from 'src/app/models/ingredient';
import { Produit } from 'src/app/models/produit';
import { ProduitService } from 'src/app/services/produit.service';
import { environment } from 'src/environments/environment';
import { CategorieService } from 'src/app/services/categorie.service';
import { IngredientService } from 'src/app/services/ingredient.service';
import { Description } from 'src/app/models/description';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-edit-produit',
  templateUrl: './edit-produit.component.html',
  styleUrls: ['./edit-produit.component.scss']
})
export class EditProduitComponent implements OnInit {
  public localhost:any= environment.apiHost+"/images/";
  public host = environment.apiHost+"/images/";
  product : any;
  ingredientsUnchecked :Ingredient[]=[];
  isLoggedIn=false
  public showAdminBoard:boolean=false;
  private roles: string[];
  
  constructor(private igredientsService : IngredientService,private activateRoute : ActivatedRoute ,
    private router :Router,private productServices : ProduitService, private _router:Router, private tokenStorageService:TokenStorageService ) { }

   ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    console.log("hhhhhhh",this.isLoggedIn)
    this.showAdminBoard = this.roles.includes('ROLE_ADMIN');

    if (!this.showAdminBoard) {
      this._router.navigateByUrl("loginAdmin")
    }else{
      this.getProduct();
    }
    console.log(this.activateRoute.snapshot.params.id)
     
     
  }
  onUnchecked(cour : Ingredient , event : any){
    if (!event.target.checked){
      this.product.ingredients = this.product.ingredients.filter(m=> m !=cour);
   
      //this.coursListCheckedDisplay = this.coursListCheckedDisplay.filter(m=> m !=cour);
      this.ingredientsUnchecked.push(cour); 
     
     }


   }
   descriptionAdded :boolean =false;

   update(){
     this.productServices.update(this.product).subscribe(data =>{
       console.log("succes");
     },err=>{
       console.log(err)
     })
     this.router.navigateByUrl("Dashboard/produit");
   }

   public addDescription(description:Description){
    if(description.nbViande==null){
      description.nbViande = 0;
    }
    this.product.description.push(description);
    this.descriptionAdded=true;
    
    setTimeout (() => {
      this.descriptionAdded=false;
   }, 3000);
  }

delete(des : any){
  this.product.description=this.product.description.filter(x => des !== x);
  
}

   ingredientsChecked : Ingredient[]=[];

  onCheckedIngredient( ingredient : Ingredient,event :any){
    if (event.target.checked){
    console.log(ingredient)
    this.product.ingredients.push(ingredient);
    this.ingredientsUnchecked = this.ingredientsUnchecked.filter(x=> x != ingredient)
    console.log(this.product.ingredients);
    
   }/*else{
    this.product.ingredients = this.product.ingredients.filter(m=> m !=ingredient);
    
    console.log(this.product.ingredients);
  }*/
  }


  async getProduct(){

    await this.productServices.findOne(this.activateRoute.snapshot.params.id).subscribe( 
      data =>{ 
        let p : Produit = data;
        this.product = p;
        console.log(p);
        console.log(this.product);
        this.igredientsService.tous_ingredients().subscribe(data =>{
          if (this.product.ingredients.length ){
            data.forEach(x => {console.log('ing',x),console.log(this.product.ingredients.includes(x.id)) })
            data.forEach(x =>{
              if(this.product.ingredients.filter(ingr =>ingr.id == x.id).length == 0){
                this.ingredientsUnchecked.push(x);
              }
            }

            )
            console.log("ingrs",this.ingredientsUnchecked)
            this.ingredientsChecked  = this.product.ingredients;
        
      }else{
          this.ingredientsUnchecked = data;
          console.log("thani",this.ingredientsUnchecked);
        }   
      },err =>{
        console.log (err)
      })
       
             
      }
      ,err =>{console.log(err)});
  }

}
