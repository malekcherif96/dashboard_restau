import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { UserAdminService } from 'src/app/services/user-admin.service';
declare var jQuery :any;
@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss']
})
export class UserAdminComponent implements OnInit {
  admins : User [] = [];
 admin : User
 message = []
 u:User
 isLoggedIn=false
 public id:string
 public showAdminBoard:boolean=false;
 private roles: string[];
  constructor(private router : Router, private tokenStorageService:  TokenStorageService  , private serviceAdmin : UserAdminService,
    private _router:Router) { }
  @ViewChild('addEmployeeModal') modaladmin :ElementRef;
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    console.log("hhhhhhh",this.isLoggedIn)
    this.showAdminBoard = this.roles.includes('ROLE_ADMIN');

    if (!this.showAdminBoard) {
      this._router.navigateByUrl("loginAdmin")
    }else{
      this.getUsers()
    }

  }
  showModal(){
    jQuery(this.modaladmin.nativeElement).modal('show');
  }
  public getUsers(){
    this.serviceAdmin.tous_admin().subscribe(res=>{
      this.admins = res;
    
      console.log(res);
    },err=>{ console.log(err);})
   
  }

  public addUser(u:User){
   
    console.log(u);
    this.u=u;
    this.serviceAdmin.save(this.u).subscribe(data=>{
      this.admin = data;  
      console.log(data)
      this.getUsers()
    },err=>{ this.message=err.message;
            (console.log(err))
    });
    jQuery(this.modaladmin.nativeElement).modal('hide');
    jQuery(this.modaladmin.nativeElement).modal('hide');
  }
  
  public deleteAdmin(id){    
    console.log("hhhhhhh"+id)
    let conf= confirm("etes vous sur de la supprimer ?");
    if(conf){
      this.serviceAdmin.deleteAdmin(id).subscribe(data=>{
        console.log(data);
        this.getUsers();
        
      },err=>{console.log(err);})
      
    }   
  }

}
