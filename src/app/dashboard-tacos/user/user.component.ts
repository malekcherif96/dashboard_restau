import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public users : User[] = [];
  public user :User
  isLoggedIn =false
  public showAdminBoard:boolean=false;
  private roles: string[];
  constructor(private router: Router, private tokenStorageService: TokenStorageService, private serviceUser:UserService,
    private _router:Router) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    console.log("hhhhhhh",this.isLoggedIn)
    this.showAdminBoard = this.roles.includes('ROLE_ADMIN');

    if (!this.showAdminBoard) {
      this._router.navigateByUrl("loginAdmin")
    }else{
      this.getUsers()
    }
    
  }


  public getUsers(){
    this.serviceUser.tous_users().subscribe(res=>{
      this.users = res;
    
      console.log(res);
    },err=>{ console.log(err);})
   
  }


}
