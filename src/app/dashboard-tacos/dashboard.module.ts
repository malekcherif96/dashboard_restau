
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { DashboardProduitComponent } from './dashboard-produit/dashboard-produit.component';
import { CategorieComponent } from './categorie/categorie.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { ExtrasComponent } from './extras/extras.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { EditProduitComponent } from './edit-produit/edit-produit.component';
import { UserComponent } from './user/user.component';
import { UserAdminComponent } from './user-admin/user-admin.component';




@NgModule({
  declarations: [CategorieComponent,DashboardProduitComponent, IngredientComponent, ExtrasComponent, EditProduitComponent, UserComponent, UserAdminComponent],
 
  imports: [
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    NgxPaginationModule
  ],
})
export class DashboardModule { }
