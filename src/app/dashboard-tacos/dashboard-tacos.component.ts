import { CommonModule } from '@angular/common';
import { Component, NgModule, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-tacos',
  templateUrl: './dashboard-tacos.component.html',
  styleUrls: ['./dashboard-tacos.component.scss']
})
export class DashboardTacosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
