import { Component, Input, OnInit, Output } from '@angular/core';
import { Ingredient } from 'src/app/models/ingredient';
import { Observable } from 'rxjs';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { IngredientService } from 'src/app/services/ingredient.service';
import { Router,ActivatedRoute } from '@angular/router';
import { EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from 'src/app/services/token-storage.service';
@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit {
  public id:string;
  //@Output() passEntry: EventEmitter<any> = new EventEmitter();
  public ingredientChoisi:Ingredient
  public ingredients:Ingredient[] = [];
  public ingredientUpdate :Ingredient
  public ingredient:Ingredient
  public i:Ingredient
  selectedFiles: FileList;
  currentFile: File;
  currentFileUpdate: File;
  progress = 0;
  message = '';
  isLoggedIn = false
  value:any;
messageSuccess = true;
public showAdminBoard:boolean=false;
private roles: string[];

  fileInfos: Observable<any>;
  public host:any=environment.apiHost+"/images/";
  constructor(private router: Router,private tokenStorageService:TokenStorageService, private serviceIngredient:IngredientService,
    private _router:Router) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    console.log("hhhhhhh",this.isLoggedIn)
    this.showAdminBoard = this.roles.includes('ROLE_ADMIN');

    if (!this.showAdminBoard) {
      this._router.navigateByUrl("loginAdmin")
    }else{
      this.getIngredients()
    }
  }

  public getIngredients(){
    this.serviceIngredient.tous_ingredients().subscribe(res=>{
      this.ingredients = res;
    
      console.log(res);
    },err=>{ console.log(err);})
   
  }

  public addIngredient(i:Ingredient){
    this.progress = 0;
    this.currentFile = this.selectedFiles.item(0);
    console.log(this.currentFile )
    this.i = i
    this.serviceIngredient.upload(this.i,this.currentFile ).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
   
        }

        setTimeout(()=>{                           //<<<---using ()=> syntax
          this.messageSuccess = false;
        }, 3000)
        this.getIngredients()
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.currentFile = null;
      });
  
    this.selectedFiles = null;

  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.currentFileUpdate=event.target.files;
  }



  public deleteIngred(id){    
    console.log("hhhhhhh"+id)
    let conf= confirm("etes vous sur de la supprimer ?");
    if(conf){
      this.serviceIngredient.delete(id).subscribe(data=>{
        console.log(data);
        this.getIngredients();
        
      },err=>{console.log(err);})
      
    }   
  }

  getIngredUpdate(id:string){
   
    console.log("id"+id )
    this.serviceIngredient.findOne(id).subscribe(data=>{
      console.log(data);
      this.id=id;
      this.ingredientUpdate = data;
    },err=>(console.log(err)));
  }
 
  updateIngred(value:Ingredient){
    console.log("******** ")
    this.progress = 0;
    this.currentFileUpdate = this.selectedFiles.item(0);
    console.log(this.currentFileUpdate )
    let ingrd:Ingredient = value;
    ingrd.id=this.id
    //ingrd = new Ingredient(this.id, value.nameIngredient, value.type);
    console.log("******** ",ingrd);
   // ingrd = new Ingredient(this.id, value.nameIngredient, value.nameIngredient, value.nameIngredient);
    this.serviceIngredient.update(ingrd,this.currentFileUpdate).subscribe(event=>{
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.message = event.body.message;
      }
      this.getIngredients()
    },err=>{
      this.progress = 0;
      this.message = 'Could not upload the file!';
      this.currentFileUpdate = null;
    });
    this.selectedFiles = null;
  }

 

}
