import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTacosComponent } from './dashboard-tacos.component';

describe('DashboardTacosComponent', () => {
  let component: DashboardTacosComponent;
  let fixture: ComponentFixture<DashboardTacosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardTacosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTacosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
