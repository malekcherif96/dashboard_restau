import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Categorie } from 'src/app/models/categorie';
import { Description } from 'src/app/models/description';
import { Ingredient } from 'src/app/models/ingredient';
import { Produit } from 'src/app/models/produit';
import { CategorieService } from 'src/app/services/categorie.service';
import { IngredientService } from 'src/app/services/ingredient.service';
import { ProduitService } from 'src/app/services/produit.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard-produit',
  templateUrl: './dashboard-produit.component.html',
  styleUrls: ['./dashboard-produit.component.scss']
})
export class DashboardProduitComponent implements OnInit {
  public host = environment.apiHost+"/images/";

  public produits:Produit[];
  public produit:Produit;
  public categories:Categorie[];
  public categorie:Categorie;
  public descriptions:Description[] = [];
  public ingredients:Ingredient[];
  public ingredientsProd:Ingredient[] = [];
  public ingredient:Ingredient;
  public descriptionAdded:boolean = false;
  public successMessage:boolean;
  public errorMessage:boolean;
  public showAdminBoard:boolean=false;

  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  message = '';
  isLoggedIn = false;
  currentIndex = -1;

  page = 1;
  count = 0;
  pageSize = 10;

  private roles: string[];

  constructor(private router:Router,private tokenStorageService:TokenStorageService, private produitService:ProduitService, private categorieService:CategorieService,
    private ingredientService:IngredientService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    console.log("hhhhhhh",this.isLoggedIn)
    this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
    if (!this.showAdminBoard) {
      this.router.navigateByUrl("loginAdmin")
    }else{
      this.getProductsByPage();
      this.getCategories();
      this.getIngredients();
      this.successMessage = false;
      this.errorMessage = false
      console.log(this.isLoggedIn)
    }
    
  }
  public onUpdate(id : number){
    this.router.navigateByUrl("Dashboard/edit/"+id);
  }

  public getProduit(){
    this.produitService.getProducts().subscribe(data=>{
      console.log(data)
      this.produits = data;
    })
  }
  getRequestParams(page, pageSize) {
    // tslint:disable-next-line:prefer-const
    let params = {};

    if (page) {
      params[`page`] = page - 1;
    }

    if (pageSize) {
      params[`size`] = pageSize;
    }

    return params;
  }

  handlePageChange(event) {
    this.page = event;
    this.getProductsByPage();
  }

  setActiveTutorial(index) {
    this.currentIndex = index;
  }

  public getProductsByPage(){
    const params = this.getRequestParams(this.page, this.pageSize);

    this.produitService.getAllPages(params)
      .subscribe(
        response => {
          const { produits, totalItems } = response;
          this.produits = produits;
          this.count = totalItems;
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }

  public getIngredients(){
    this.ingredientService.tous_ingredients().subscribe(data=>{
      this.ingredients = data;
    })
  }

  public getCategories(){
    this.categorieService.tous_categories().subscribe(data=>{
      this.categories = data;
      console.log(data);
    })
  }

  onSelectedCategorie(categorie: Categorie){
    console.log(categorie);
    this.categorie = categorie;
    }

  onCheckedIngredient( ingredient : Ingredient,event :any){
    if (event.target.checked){
    console.log(ingredient)
    this.ingredientsProd.push(ingredient);
    console.log(this.ingredientsProd);
   }else{
    this.ingredientsProd = this.ingredientsProd.filter(m=> m !=ingredient);
    console.log(this.ingredientsProd);
  }
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  addProduit(produit:Produit) {
    this.progress = 0;
    this.produit=produit;
    this.produit.categorie = this.categorie;
    this.produit.description = this.descriptions;
    this.produit.ingredients = this.ingredientsProd;
    console.log(produit);
    this.currentFile = this.selectedFiles.item(0);
    this.produitService.upload(this.produit,this.currentFile).subscribe(
      event => {
        console.log(event)
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
          this.produit.imageProduit=this.currentFile.name
          this.successMessage = true;
          this.errorMessage = false;
          
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
          console.log(this.message)
        }
        this.getProductsByPage();
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.currentFile = undefined;
        this.errorMessage = true;
        this.successMessage = false;
      });
    this.selectedFiles = undefined;
  }

  public addDescription(description:Description){
    if(description.nbViande==null){
      description.nbViande = 0;
    }
    this.descriptions.push(description);
    this.descriptionAdded=true;
    console.log("*********",this.descriptions);
    setTimeout (() => {
      this.descriptionAdded=false;
   }, 3000);
  }

  public deleteProduit(id:number){
    let conf= confirm("etes vous sur de la supprimer ?");
    if(conf){
      this.produitService.deleteProduit(id).subscribe(data=>{
        console.log(data);
        this.produits = this.produits.filter(p => p.id != id);
        
      },err=>{console.log(err);})
      
    }
  }
  public onUpdateProduct(id : number){
    this.router.navigateByUrl("/edit/"+id);
  }




}
