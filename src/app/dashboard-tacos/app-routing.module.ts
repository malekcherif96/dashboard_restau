import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardProduitComponent } from './dashboard-produit/dashboard-produit.component';
import { CategorieComponent } from './categorie/categorie.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { ExtrasComponent } from './extras/extras.component';
import { EditProduitComponent } from './edit-produit/edit-produit.component';
import { UserComponent } from './user/user.component';
import { UserAdminComponent } from './user-admin/user-admin.component';

const routes: Routes = [
 {path:'', redirectTo:'Admin', pathMatch:'full'},
  {path:'produit', component:DashboardProduitComponent},
  {path:'categorie', component:CategorieComponent},
  {path:'ingredient', component:IngredientComponent},
  {path:'extras', component:ExtrasComponent},
  {path:'edit/:id', component:EditProduitComponent},
  {path:'users', component:UserComponent},
  {path:'admins', component:UserAdminComponent},
 
];

@NgModule({
  imports: [RouterModule.forChild(routes),
],
  exports: [RouterModule]
})

export class AppRoutingModule { }
