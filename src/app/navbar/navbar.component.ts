import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from '../services/token-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isLoggedIn =false
  public roles: string[];
  public showAdminBoard:boolean=false;
  constructor(private _router:Router, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn){
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
      console.log("hhhhhhh",this.isLoggedIn)
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
    }
    

    if (!this.showAdminBoard) {
      this._router.navigateByUrl("loginAdmin")
    }
  }
  logout(): void {
    this.tokenStorageService.signOut();
    this._router.navigateByUrl("loginAdmin");
    window.location.reload()
  }

}
