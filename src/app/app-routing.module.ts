import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardTacosComponent } from './dashboard-tacos/dashboard-tacos.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [

  {path:"loginAdmin",component:RegisterComponent},
  {path:"navbar",component:NavbarComponent},

  {path:'', redirectTo:'', pathMatch:'full'},
  {path:'Dashboard', loadChildren:() => import('./dashboard-tacos/dashboard.module').then(m => m.DashboardModule)}


];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    paramsInheritanceStrategy: 'always'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
