import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { TokenStorageService } from '../services/token-storage.service';
import { UserAdmin } from '../services/user-admin';
import { UserAdminService } from '../services/user-admin.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  public roles: string[];
  public showAdminBoard:boolean=false;
  constructor( private _router: Router,private auth:UserAdminService,private tokenStorage :TokenStorageService  ) { }

  ngOnInit(): void {
    console.log("HELLO")
    this.isLoggedIn = !!this.tokenStorage.getToken();
    const user = this.tokenStorage.getUser();
    if(user != null){
      this.roles = user.roles;
      console.log("hhhhhhh",this.isLoggedIn)
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
    }
    

    if (!this.showAdminBoard) {
      this._router.navigateByUrl("loginAdmin")
    }else{
      console.log(this.isLoggedIn)
    }
  }

  onSubmit(): void {
    this.auth.login(this.form).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);
        let user:User = data;

        this.roles = this.tokenStorage.getUser().roles;
        
        if(!this.roles.includes("ROLE_ADMIN")){
          this.tokenStorage.signOut();
          console.log("ROLE_ADMIN")
          this.isLoginFailed = true;
          this.isLoggedIn = false;
          this.errorMessage = "Utilisateur Non Autorisé"
          this.reloadPage();
        }else{
          this.isLoginFailed = false;
          this.isLoggedIn = true;
          this.reloadPage();
        }
        
        //this.reloadPage();
        
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  reloadPage(): void {
      this._router.navigateByUrl("Dashboard");
      console.log("5555555")
      window.location.reload();
    
  }

 
  
}
