import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  host:string = environment.apiHost+"/api/auth";
  constructor(private Http:HttpClient) { }
  public tous_users(){
    return this.Http.get<User[]>(this.host+"/get");
  }

 

  
  
}
