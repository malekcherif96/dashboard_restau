import {Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { CurdOperations } from '../interfaces/curd-operations';
@Injectable({
  providedIn: 'root'
})
export class CrudService<T,ID>implements CurdOperations<T,ID> {

  constructor(private _http: HttpClient , @Inject(String)private _base: string) { }

  save(t: T): Observable<T> {
    return this._http.post<T>(this._base+"/add/",t);
  }

  update(id: ID, t: T): Observable<T> {
    return this._http.put<T>(this._base+"/update/"+id,t);
  }

  findOne(id: ID): Observable<T> {
    return this._http.get<T>(this._base+"/getOne/"+id);
  }
  findAll(): Observable<T[]> {
    return this._http.get<T[]>(this._base+"/getAll/");
  }
  delete(id: ID): Observable<any> {
    return this._http.delete<T>(this._base+"/delete/"+id);
  }
}
