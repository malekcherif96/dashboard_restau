import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Extras } from '../models/extras';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ExtrasService {

  host:string = environment.apiHost+"/extra";
  constructor(private Http:HttpClient) { }


  public tous_extras(){
    return this.Http.get<Extras[]>(this.host+"/get");
  }

 public delete(id:string){
    return this.Http.delete<Extras>(this.host+"/delete/"+id);
}
  public findOne(id:string){
    return this.Http.get<Extras>(this.host+"/get/"+id);
  }

  upload(extras:Extras ,file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    console.log(JSON.stringify(extras))
    formData.append('file', file);
    formData.append('extras',JSON.stringify(extras));

    const req = new HttpRequest('POST', `${this.host}`+"/save", formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.Http.request(req);
  }

  update(extras:Extras ,file: File):Observable<HttpEvent<any>>{
    const formData: FormData = new FormData();
    console.log(JSON.stringify(extras))
    formData.append('file', file);
    formData.append('extras',JSON.stringify(extras));

    const req = new HttpRequest('PUT', `${this.host}`+"/update", formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.Http.request(req);
}
}
