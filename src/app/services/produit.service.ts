import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Produit } from '../models/produit';
import { environment } from 'src/environments/environment';

const API_HOST = environment.apiHost+'/produit'
@Injectable({
  providedIn: 'root'
})
export class ProduitService {
  //host:string = "http://localhost:8090/produit";

  constructor(private http:HttpClient) { }

  public upload(produit:Produit, file:File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    console.log(JSON.stringify(produit))
    formData.append('file', file);
    formData.append('produit', JSON.stringify(produit));
    const req = new HttpRequest('POST', `${API_HOST}/save`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
  public findOne(id:any):Observable<any>{
    return this.http.get<any>(API_HOST+"/get/"+id);
  }
  public update(produit:Produit):Observable<any>{
    return this.http.put<any>(API_HOST+"/update/",produit);
  }
  public getProducts():Observable<Produit[]>{
    return this.http.get<Produit[]>(API_HOST+"/get");
  }

  public getAllPages(params): Observable<any> {
    return this.http.get(API_HOST+"/pages", { params });
  }

  public deleteProduit(id:number):Observable<any>{
    return this.http.delete(API_HOST+"/delete/"+id);
  }



 


}
