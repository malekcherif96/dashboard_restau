import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class UserAdminService {
  host:string = environment.apiHost+"/api/auth";
  constructor(private Http:HttpClient) { }

  save(t:User):Observable<User>{
    return this.Http.post<User>(this.host+"/signup/Admin", t);
  }

public tous_admin(){
  return this.Http.get<User[]>(this.host+"/get/admin");
}
public deleteAdmin(id:number):Observable<any>{
  return this.Http.delete(this.host+"/delete/"+id);
}



login(user :User): Observable<any> {
  return this.Http.post(this.host + '/signin',user);
}
}
