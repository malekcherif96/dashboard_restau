import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Categorie } from '../models/categorie';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  host:string = environment.apiHost+"/categorie";
  constructor(private Http:HttpClient) { }


  public tous_categories(){
    return this.Http.get<Categorie[]>(this.host+"/get")
  }

  public delete(id:string){
    return this.Http.delete<Categorie>(this.host+"/delete/"+id);
}

  upload(category:Categorie ,file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    console.log(JSON.stringify(category))
    formData.append('file', file);
    formData.append('category',JSON.stringify(category));

    const req = new HttpRequest('POST', `${this.host}`+"/save", formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.Http.request(req);
  }

  public findOne(id:number){
    return this.Http.get<Categorie>(this.host+"/get/"+id);
  }
  update(category:Categorie ,file: File):Observable<HttpEvent<any>>{
    const formData: FormData = new FormData();
    console.log(JSON.stringify(category))
    formData.append('file', file);
    formData.append('category',JSON.stringify(category));

    const req = new HttpRequest('PUT', `${this.host}`+"/update", formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.Http.request(req);
}


  

}
