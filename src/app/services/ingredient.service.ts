import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Ingredient } from '../models/ingredient';


@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  host:string = environment.apiHost+"/ingredient";
  constructor(private Http:HttpClient) { }


  public tous_ingredients(){
    return this.Http.get<Ingredient[]>(this.host+"/get/");
  }

 public delete(id:string){
    return this.Http.delete<Ingredient>(this.host+"/"+id);
}
  public findOne(id:string){
    return this.Http.get<Ingredient>(this.host+"/get/"+id);
  }

  upload(ingredient:Ingredient ,file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    console.log(JSON.stringify(ingredient))
    formData.append('file', file);
    formData.append('ingredient',JSON.stringify(ingredient));

    const req = new HttpRequest('POST', `${this.host}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.Http.request(req);
  }

  update(ingredient:Ingredient ,file: File):Observable<HttpEvent<any>>{
    const formData: FormData = new FormData();
    console.log(JSON.stringify(ingredient))
    formData.append('file', file);
    formData.append('ingredient',JSON.stringify(ingredient));

    const req = new HttpRequest('PUT', `${this.host}`+"/update", formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.Http.request(req);
}
}
