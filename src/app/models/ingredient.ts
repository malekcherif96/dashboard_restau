export class Ingredient {
    id: string;
    nameIngredient: string;
    image: string;
    type: string;

    constructor(id:string,nameIngredient:string,type:string){
        this.id = id;
        this.nameIngredient = nameIngredient;
        this.type = type;
    }

}
