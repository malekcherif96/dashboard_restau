import { Categorie } from "./categorie"

export class Extras {
    id:string
    nomExtras:string
    imageExtras:string
    price:number
    categorie:Categorie
}
