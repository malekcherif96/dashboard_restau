import { Order } from "./order"
import { Role } from "./role"

export class User {
    id : string
    username: string
    password : string
    email: string
    numTel: number
    adresse : string
    nomPrenom:string
    orders : Order []
    roles : Role[]
}

