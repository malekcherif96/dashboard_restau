import { Orderitem } from "./orderitem"
import { User } from "./user"

export class Order {
    id : number 
    items : Orderitem []
    tableR : User[]
    totalAmount : number
    ordered : boolean
    traiter : boolean
}
