import { Categorie } from "./categorie";
import { Description } from "./description";
import { Ingredient } from "./ingredient";
import { Orderitem } from "./orderitem";

export class Produit {
    id:number
    nomProduit:string;
    imageProduit:string;
    categorie:Categorie;
    ingredients:Ingredient[];
    description:Description[];
    orderItems:Orderitem[];
}

